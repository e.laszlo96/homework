<?php

use App\Exceptions\MissingRequiredSubjectException;
use App\Validator\BasicRequiredSubjectValidator;
use Test\StudentCreatorTestCase;

class BasicRequiredSubjectValidatorTest extends StudentCreatorTestCase {
    private BasicRequiredSubjectValidator $underTest;

    public function setUp(): void {
        $this->underTest = new BasicRequiredSubjectValidator();
    }

    public function test_it_should_return_true() {

        //GIVEN        
        $student = $this->createStudent(null, [
            ["name" => "matematika"],
            ["name" => "magyar nyelv és irodalom"],
            ["name" => "történelem"]
        ]);
        //WHEN
        $bool = $this->underTest->isValid($student);
        //THEN
        return $this->assertTrue($bool);
    }

    public function test_it_should_return_false() {
        //GIVEN
        $student = $this->createStudent(null, [
            ["name" => "informatika"],
            ["name" => "magyar nyelv és irodalom"],
            ["name" => "történelem"]
        ]);

        $this->expectException(MissingRequiredSubjectException::class);
        //WHEN
        $this->underTest->isValid($student);
        //THEN
    }

    public function test_it_should_return_false2() {
        //GIVEN
        $student = $this->createStudent(null, [
            ["name" => "matematika"],
            ["name" => "magyar nyelv és irodalom"]
        ]);
        //WHEN
        $this->expectException(MissingRequiredSubjectException::class);
        //THEN
        $this->underTest->isValid($student);
    }
}

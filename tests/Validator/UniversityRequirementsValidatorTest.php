<?php

use App\Validator\UniversityRequirementsValidator;
use Test\StudentCreatorTestCase;

class UniversityRequirementsValidatorTest extends StudentCreatorTestCase {
    private $underTest;
    private $config;

    public function setUp(): void {
        require(__DIR__ . "/../config.php");
        $this->config = $config;
        $this->underTest = new UniversityRequirementsValidator($this->config["university"]);
    }



    public function test_level_should_return_true() {

        //GIVEN     
        $student = $this->createStudent(["ELTE", "IK"], [
            [
                "name" => "matematika",
                "type" => "közép"
            ],
            [
                "name" => "informatika",
                "type" => "közép"
            ],
        ]);
        //WHEN
        //THEN
        return $this->assertTrue($this->underTest->isValid($student));
    }

    public function test_level_should_return_true_if_level_is_better() {

        //GIVEN  
        $student = $this->createStudent(["ELTE", "IK"], [
            [
                "name" => "matematika",
                "type" => "emelt"
            ],
            [
                "name" => "informatika",
                "type" => "emelt"
            ],
        ]);
        //WHEN
        //THEN
        return $this->assertTrue($this->underTest->isValid($student));
    }


    public function test_level_should_return_true_if_level_is_better2() {

        //GIVEN  
        $student = $this->createStudent(["PPKE", "BTK"],  [
            [
                "name" => "angol",
                "type" => "emelt"
            ],
            [
                "name" => "német",
                "type" => "emelt"
            ],
        ]);
        //WHEN
        //THEN
        return $this->assertTrue($this->underTest->isValid($student));
    }

    public function test_level_should_return_false_if_level_is_lower() {

        //GIVEN       
        $student = $this->createStudent(["PPKE", "BTK"], [
            [
                "name" => "angol",
                "type" => "közép"
            ],
            [
                "name" => "német",
                "type" => "emelt"
            ],
        ]);
        //WHEN
        //THEN
        return $this->assertFalse($this->underTest->isValid($student));
    }



    public function test_subject_should_return_false_if_other_subject() {

        //GIVEN        
        $student = $this->createStudent(["PPKE", "BTK"], [
            [
                "name" => "német",
                "type" => "emelt"
            ],
            [
                "name" => "német",
                "type" => "emelt"
            ],
        ]);
        //WHEN
        //THEN
        return $this->assertFalse($this->underTest->isValid($student));
    }

    public function test_subject_should_return_false_if_other_subject2() {

        //GIVEN  
        $student = $this->createStudent(["PPKE", "BTK"], [
            [
                "name" => "angol",
                "type" => "emelt"
            ],
            [
                "name" => "angol",
                "type" => "emelt"
            ],
        ]);
        //WHEN
        //THEN
        return $this->assertFalse($this->underTest->isValid($student));
    }
}

<?php

use App\Exceptions\TooLowResultException;
use App\Validator\CheckMinResultValidator;
use Test\StudentCreatorTestCase;

class CheckMinResultValidatorTest extends StudentCreatorTestCase {
    private CheckMinResultValidator $underTest;

    public function setUp():void{
        
        $this->underTest = new CheckMinResultValidator();
    }

    public function test_it_should_return_true(){
        //GIVEN        
        $student = $this->createStudent(null, [
            ["result" => 20],
            ["result" => 21],
            ["result" => 20],
        ]);
        //WHEN
        $bool = $this->underTest->isValid($student);
        //THEN
        return $this->assertTrue($bool);
    }  
    
    public function test_it_should_return_false(){
        //GIVEN        
        $student = $this->createStudent(null, [
            ["result" => 20],
            ["result" => 21],
            ["result" => 19],
        ]);
        //WHEN
        $this->expectException(TooLowResultException::class);
        //THEN
        $bool = $this->underTest->isValid($student);
    }  

}

?>
<?php

namespace Test;

use App\Model\LanguageExam;
use App\Model\Student;
use App\Model\Subject;
use App\Model\University;
use PHPUnit\Framework\TestCase;

abstract class StudentCreatorTestCase extends TestCase {


    protected function createStudent(array $university = null, array $subjects = null, array $languages = null) {
        $student = (new Student());

        if ($university) {
            $student->setTargetUniversity(
                $this->createUniversityMockWithReturnParameter($university[0], $university[1])
            );
        }

        if ($subjects) {
            foreach ($subjects as $subject) {
                $s = $this->createMock(Subject::class);

                if (isset($subject["name"]) && !empty($subject["name"])) {
                    $s->method("getName")->willReturn($subject["name"]);
                }
                if (isset($subject["type"]) && !empty($subject["type"])) {
                    $s->method("getType")->willReturn($subject["type"]);
                }
                if (isset($subject["result"]) && !empty($subject["result"])) {
                    $s->method("getResult")->willReturn($subject["result"]);
                }
                $student->addSubject($s);
            }
        }

        if ($languages) {
            foreach ($languages as $language) {
                $s = $this->createMock(LanguageExam::class);
                $s->method("getName")->willReturn($language["name"]);
                $s->method("getType")->willReturn($language["type"]);
                $student->addLanguageExams($s);
            }
        }
        
        return $student;
    }

    private function createUniversityMockWithReturnParameter(String $a, String $b) {
        $s = $this->createMock(University::class);
        $s->method("getName")->willReturn($a);
        $s->method("getFaculty")->willReturn($b);
        return $s;
    }
}

<?php

use App\Model\Result;
use App\View;
use PHPUnit\Framework\TestCase;

class ViewTest extends TestCase
{
    private $target;

    public function setUp():void{
        $this->target = new View();
    }

    public function test_it_should_assert_string()
    {
        $result = new Result(20, 10);
        
        return $this->assertEquals("30 (20 alappont + 10 többletpont)", $this->target->emit($result));
    }
}

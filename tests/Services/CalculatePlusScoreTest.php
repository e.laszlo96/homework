<?php

use App\Services\CalculatePlusScore;
use Test\StudentCreatorTestCase;

class CalculatePlusScoreTest extends StudentCreatorTestCase {
    private CalculatePlusScore $underTest;

    public function setUp(): void {

        $this->underTest = new CalculatePlusScore();
    }

    public function test_it_should_return_50() {
        //GIVEN        
        $student = $this->createStudent(null, [
            [
                "name" => "matematika",
                "type" => "emelt"
            ]
        ]);
        //WHEN
        $result = $this->underTest->calc($student);
        //THEN
        return $this->assertEquals(50, $result);
    }


    public function test_it_should_return_100() {
        //GIVEN        
        $student = $this->createStudent(null, [
            [
                "name" => "matematika",
                "type" => "emelt"
            ],
            [
                "name" => "informatika",
                "type" => "emelt"
            ],
            [
                "name" => "informatika",
                "type" => "emelt"
            ]
        ]);
        //WHEN
        $result = $this->underTest->calc($student);
        //THEN
        return $this->assertEquals(100, $result);
    }

    public function test_it_should_return_0() {
        //GIVEN        
        $student = $this->createStudent(null, [
            [
                "name" => "matematika",
                "type" => "közép"
            ],
            [
                "name" => "informatika",
                "type" => "közép"
            ],
            [
                "name" => "informatika",
                "type" => "közép"
            ]
        ]);
        //WHEN
        $result = $this->underTest->calc($student);
        //THEN
        return $this->assertEquals(0, $result);
    }

    public function test_it_should_return_28() {
        //GIVEN        
        $student = $this->createStudent(null, null, [
            [
                "name" => "angol",
                "type" => "B2"
            ]
        ]);
        //WHEN
        $result = $this->underTest->calc($student);
        //THEN
        return $this->assertEquals(28, $result);
    }

    public function test_it_should_return_40() {
        //GIVEN        
        $student = $this->createStudent(null, null, [
            [
                "name" => "angol",
                "type" => "C2"
            ],
            [
                "name" => "angol",
                "type" => "B2"
            ]
        ]);
        //WHEN
        $result = $this->underTest->calc($student);
        //THEN
        return $this->assertEquals(40, $result);
    }


    public function test_it_should_return_40_2() {
        //GIVEN        
        $student = $this->createStudent(null, null, [
            [
                "name" => "angol",
                "type" => "B2"
            ],
            [
                "name" => "angol",
                "type" => "C2"
            ]
        ]);
        //WHEN
        $result = $this->underTest->calc($student);
        //THEN
        return $this->assertEquals(40, $result);
    }

    public function test_it_should_return_80() {
        //GIVEN        
        $student = $this->createStudent(null, null, [
            [
                "name" => "angol",
                "type" => "B2"
            ],
            [
                "name" => "angol",
                "type" => "C2"
            ],
            [
                "name" => "német",
                "type" => "C2"
            ]
        ]);
        //WHEN
        $result = $this->underTest->calc($student);
        //THEN
        return $this->assertEquals(80, $result);
    }

    public function test_it_should_return_68() {
        //GIVEN        
        $student = $this->createStudent(null, null, [
            [
                "name" => "angol",
                "type" => "B2"
            ],
            [
                "name" => "német",
                "type" => "C2"
            ]
        ]);
        //WHEN
        $result = $this->underTest->calc($student);
        //THEN
        return $this->assertEquals(68, $result);
    }

    public function test_it_should_return_78() {
        //GIVEN        
        $student = $this->createStudent(null, [
            [
                "name" => "matematika",
                "type" => "emelt"
            ],
        ], [
            [
                "name" => "angol",
                "type" => "B2"
            ]
        ]);
        //WHEN
        $result = $this->underTest->calc($student);
        //THEN
        return $this->assertEquals(78, $result);
    }

    public function test_it_should_return_90() {
        //GIVEN        
        $student = $this->createStudent(null, [
            [
                "name" => "matematika",
                "type" => "emelt"
            ],
        ], [
            [
                "name" => "angol",
                "type" => "C2"
            ]
        ]);
        //WHEN
        $result = $this->underTest->calc($student);
        //THEN
        return $this->assertEquals(90, $result);
    }
}

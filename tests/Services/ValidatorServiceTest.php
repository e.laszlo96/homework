<?php

use App\Exceptions\MissingRequiredSubjectException;
use App\Exceptions\TooLowResultException;
use App\Services\ValidatorService;
use App\Validator\BasicRequiredSubjectValidator;
use Test\StudentCreatorTestCase;
use App\Validator\CheckMinResultValidator;
use App\Validator\UniversityRequirementsValidator;

class ValidatorServiceTest extends StudentCreatorTestCase {
    private ValidatorService $underTest;
    private $config;

    public function setUp(): void {

        require(__DIR__ . "/../config.php");
        $this->config = $config;

        $this->underTest = new ValidatorService();
        $this->underTest
            ->addValidator(new CheckMinResultValidator())
            ->addValidator(new BasicRequiredSubjectValidator())
            ->addValidator(new UniversityRequirementsValidator($this->config["university"]));
    }

    public function test_it_should_return_true() {
        //GIVEN        
        $student = $this->createStudent(["PPKE", "BTK"], [
            [
                "name" => "angol",
                "type" => "emelt",
                "result" => 20
            ],
            [
                "name" => "német",
                "type" => "közép",
                "result" => 20
            ],
            [
                "name" => "matematika",
                "type" => "közép",
                "result" => 20
            ],
            [
                "name" => "magyar nyelv és irodalom",
                "type" => "közép",
                "result" => 20
            ],
            [
                "name" => "történelem",
                "type" => "közép",
                "result" => 20
            ]
        ]);
        //WHEN
        $bool = $this->underTest->isValid($student);
        //THEN
        return $this->assertTrue($bool);
    }

    public function test_it_should_return_false_because_result_is_too_low() {
        //GIVEN        
        $student = $this->createStudent(["PPKE", "BTK"], [
            [
                "name" => "angol",
                "type" => "emelt",
                "result" => 20
            ],
            [
                "name" => "német",
                "type" => "közép",
                "result" => 20
            ],
            [
                "name" => "matematika",
                "type" => "közép",
                "result" => 20
            ],
            [
                "name" => "magyar nyelv és irodalom",
                "type" => "közép",
                "result" => 20
            ],
            [
                "name" => "történelem",
                "type" => "közép",
                "result" => 19
            ]
        ]);
        //WHEN
        $this->expectException(TooLowResultException::class);
        //THEN
        $this->underTest->isValid($student);
    }

    public function test_it_should_return_false_because_level_is_too_low() {
        //GIVEN        
        $student = $this->createStudent(["PPKE", "BTK"], [
            [
                "name" => "angol",
                "type" => "közép",
                "result" => 20
            ],
            [
                "name" => "német",
                "type" => "emelt",
                "result" => 20
            ],
            [
                "name" => "matematika",
                "type" => "közép",
                "result" => 20
            ],
            [
                "name" => "magyar nyelv és irodalom",
                "type" => "közép",
                "result" => 20
            ],
            [
                "name" => "történelem",
                "type" => "közép",
                "result" => 20
            ]
        ]);
        //WHEN
        $bool = $this->underTest->isValid($student);
        //THEN
        return $this->assertFalse($bool);
    }

    public function test_it_should_return_false_because_subjects_are_too_few() {
        //GIVEN        
        $student = $this->createStudent(
            ["PPKE", "BTK"],
            [
                [
                    "name" => "angol",
                    "type" => "emelt",
                    "result" => 20
                ],
                [
                    "name" => "német",
                    "type" => "közép",
                    "result" => 20
                ],
                [
                    "name" => "matematika",
                    "type" => "közép",
                    "result" => 20
                ],
                [
                    "name" => "magyar nyelv és irodalom",
                    "type" => "közép",
                    "result" => 20
                ]
            ]
        );
        //WHEN
        $this->expectException(MissingRequiredSubjectException::class);
        //THEN
        $this->underTest->isValid($student);
    }
}

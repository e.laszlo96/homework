<?php

use App\Services\CalculateBasicScore;
use Test\StudentCreatorTestCase;

class CalculateBasicScoreTest extends StudentCreatorTestCase {
    private CalculateBasicScore $underTest;
    private $config;

    public function setUp(): void {

        require(__DIR__ . "/../config.php");
        $this->config = $config;
        $this->underTest = new CalculateBasicScore($config["university"]);
      
    }

    public function test_it_should_return_100() {
        //GIVEN        
        $student = $this->createStudent(["ELTE", "IK"], [
            [
                "name" => "matematika",
                "type" => "közép",
                "result" => 25
            ],
            [
                "name" => "fizika",
                "type" => "közép",
                "result" => 25
            ],
            [
                "name" => "informatika",
                "type" => "közép",
                "result" => 20
            ]
        ]);
        //WHEN
        $result = $this->underTest->calc($student);
        //THEN
        return $this->assertEquals(100, $result);
    }

    public function test_it_should_return_150() {
        //GIVEN        
        $student = $this->createStudent(["ELTE", "IK"], [
            [
                "name" => "matematika",
                "type" => "közép",
                "result" => 25
            ],
            [
                "name" => "fizika",
                "type" => "közép",
                "result" => 20
            ],
            [
                "name" => "informatika",
                "type" => "közép",
                "result" => 50
            ]
        ]);
        //WHEN
        $result = $this->underTest->calc($student);
        //THEN
        return $this->assertEquals(150, $result);
    }

    public function test_it_should_return_0() {
        //GIVEN        
        $student = $this->createStudent(["PPKE", "BTK"], [
            [
                "name" => "matematika",
                "type" => "közép",
                "result" => 25
            ],
            [
                "name" => "fizika",
                "type" => "közép",
                "result" => 25
            ],
            [
                "name" => "informatika",
                "type" => "közép",
                "result" => 20
            ]
        ]);
        //WHEN
        $result = $this->underTest->calc($student);
        //THEN
        return $this->assertEquals(0, $result);
    }

   
}

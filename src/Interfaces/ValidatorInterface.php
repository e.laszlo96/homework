<?php 

namespace App\Interfaces;

use App\Model\Student;

interface ValidatorInterface{
    public function isValid(Student $s): bool;
}

?>
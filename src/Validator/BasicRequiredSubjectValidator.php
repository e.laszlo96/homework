<?php 

namespace App\Validator;

use App\Exceptions\MissingRequiredSubjectException;
use App\Interfaces\ValidatorInterface;
use App\Model\Student;

class BasicRequiredSubjectValidator implements ValidatorInterface {
    const REQUIREMENTS = ["magyar nyelv és irodalom", "történelem", "matematika"];

    public function isValid(Student $s): bool{

        foreach (self::REQUIREMENTS as $value) {
            $bool = false;
            foreach ($s->getSubjects() as $subject) {
                if ($subject->getName() == $value) {
                    $bool = true;
                }
            }
            if (!$bool) {
                throw new MissingRequiredSubjectException();
                return false;
            }
        }
        return true;
    }
}

?>
<?php 

namespace App\Validator;

use App\Exceptions\TooLowResultException;
use App\Interfaces\ValidatorInterface;
use App\Model\Student;

class CheckMinResultValidator implements ValidatorInterface {
    const MIN = 20;

    public function isValid(Student $s): bool{
        
        foreach ($s->getSubjects() as $subject) {
            if ($subject->getResult() < self::MIN) {
                throw new TooLowResultException();
                
            }
        }
        return true;
    }
}

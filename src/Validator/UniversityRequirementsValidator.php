<?php

namespace App\Validator;

use App\Interfaces\ValidatorInterface;
use App\Model\Student;

class UniversityRequirementsValidator implements ValidatorInterface {

    private array $requirements;

    public function __construct(array $requirements) {
        $this->requirements = $requirements;
    }

    public function isValid(Student $s): bool {
        return ($this->checkRequired($s) && $this->checkOptions($s));
    }

    private function checkOptions(Student $s):bool {
        $university = $s->getTargetUniversity();
        $options = $this->requirements[$university->getName()][$university->getFaculty()]["optional"];
        
        foreach ($options as $option) {
            foreach ($s->getSubjects() as $subject) {
                if ($subject->getName() == $option["name"]) {
                    if ($option["level"] == "közép" || ($option["level"] == "emelt" && $subject->getType() == "emelt")) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private function checkRequired(Student $s):bool {
        $university = $s->getTargetUniversity();
        $required = $this->requirements[$university->getName()][$university->getFaculty()]["required"];
       
        foreach ($required as $require) {
            $bool = false;
            foreach ($s->getSubjects() as $subject) {
                if ($subject->getName() == $require["name"]) {
                    if ($require["level"] == "közép") {
                        $bool = true;
                    }
                    if ($require["level"] == "emelt" && $subject->getType() == "emelt") {
                        $bool = true;
                    }
                }
            }
            if (!$bool) {
                return false;
            }
        }
        return $bool;
    }
}

<?php 

namespace App;

use App\Model\Result;

class View
{
    public function emit(Result $result){
        return sprintf("%d (%d alappont + %d többletpont)", $result->getSumScore(), $result->getBaseScore(), $result->getPlusScore());
    }
}

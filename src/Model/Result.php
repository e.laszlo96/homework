<?php

namespace App\Model;

final class Result {

    private int $baseScore;
    private int $plusScore;
    private int $sumScore;

    public function __construct(int $baseScore, int $plusScore) {
        $this->baseScore = $baseScore;
        $this->plusScore = $plusScore;
        $this->sumScore = $baseScore + $plusScore;
    }

    public function getBaseScore(): int {
        return $this->baseScore;
    }
    public function getPlusScore(): int {
        return $this->plusScore;
    }
    public function getSumScore(): int {
        return $this->sumScore;
    }
}

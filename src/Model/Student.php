<?php

namespace App\Model;


class Student {
    private University $targetUniversity;

    /**
     * @var Array<Subject>
     */
    private array $subjects = [];

    /**
     * @var Array<LanguageExam>
     */
    private array $languageExams = [];


    public function getTargetUniversity(): University {
        return $this->targetUniversity;
    }

    public function setTargetUniversity($targetUniversity) {
        $this->targetUniversity = $targetUniversity;

        return $this;
    }

    public function addSubject(Subject $subject): self {
        $this->subjects[] = $subject;
        return $this;
    }

    public function getSubjects(): array {
        return $this->subjects;
    }

    public function addLanguageExams(LanguageExam $exam): self {
        $this->languageExams[] = $exam;
        return $this;
    }

    public function getLanguageExams(): array {
        return $this->languageExams;
    }
}

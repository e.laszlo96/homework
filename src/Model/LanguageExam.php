<?php

namespace App\Model;

class LanguageExam {

    private String $name;
    private String $type;


    public function getName(): String {
        return $this->name;
    }

    public function setName(String $name): self {
        $this->name = $name;

        return $this;
    }

    public function getType(): String {
        return $this->type;
    }

    public function setType(String $type): self {
        $this->type = $type;

        return $this;
    }
}

<?php

namespace App\Model;

class University {
    private String $name;
    private String $course;
    private String $faculty;

    public function getName(): String {
        return $this->name;
    }

    public function setName(String $name): self {
        $this->name = $name;
        return $this;
    }

    public function getCourse(): String {
        return $this->course;
    }

    public function setCourse(String $course): self {
        $this->course = $course;
        return $this;
    }

    public function getFaculty(): String {
        return $this->faculty;
    }

    public function setFaculty(String $faculty): self {
        $this->faculty = $faculty;
        return $this;
    }
}

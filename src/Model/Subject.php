<?php

namespace App\Model;

use App\Services\SubjectTransformer;

class Subject {
    private String $name;
    private String $type;
    private Int $result;

    public function __construct(SubjectTransformer $subjectTransformer) {
        $this->name = $subjectTransformer->getName();
        $this->type = $subjectTransformer->getType();
        $this->result = $subjectTransformer->getResult();
    }

    public function getName(): String {
        return $this->name;
    }

    public function getType(): String {
        return $this->type;
    }
    public function getResult(): Int {
        return $this->result;
    }
    static public function getTransformer() {
        return (new SubjectTransformer());
    }
}

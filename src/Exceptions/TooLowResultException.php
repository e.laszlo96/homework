<?php 

namespace App\Exceptions;

class TooLowResultException extends \Exception
{
    public function __construct() {
        parent::__construct("Hiba, nem lehetséges a pontszámítás. Egy vagy több tantárgyból elért 20% alatti eredmény miatt!");
    }
}

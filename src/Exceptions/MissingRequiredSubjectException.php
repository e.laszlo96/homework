<?php 

namespace App\Exceptions;

class MissingRequiredSubjectException extends \Exception
{
    public function __construct() {
        parent::__construct("Nem lehetséges a pontszámítás a kötelező érettségi tárgyak hiánya miatt!");
    }
}

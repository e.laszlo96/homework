<?php 

namespace App\Services;

use App\Model\Student;
use App\Model\University;
use App\Model\Subject;
use App\Model\LanguageExam;

class StudentCreator
{
    static public function do(Array $data):Student
    {
        $student = new Student();
        
        $university = (new University())
            ->setName($data['valasztott-szak']["egyetem"])
            ->setFaculty($data['valasztott-szak']["kar"])
            ->setCourse($data['valasztott-szak']["szak"]);

        $student->setTargetUniversity($university);


        foreach ($data["erettsegi-eredmenyek"] as $value) {
            $subject = Subject::getTransformer()->setName($value["nev"])->setType($value["tipus"])->setResult($value["eredmeny"])->build();
            $student->addSubject($subject);
        }

        foreach ($data["tobbletpontok"] as $value) {
            $exam = (new LanguageExam())->setName($value["nyelv"])->setType($value["tipus"]);
            $student->addLanguageExams($exam);
        }

        return $student;
    }
}


?>
<?php

namespace App\Services;

use App\Model\Subject;

class SubjectTransformer {

    private String $name;
    private String $type;
    private Int $result;

    public function setName(String $name): self {
        $this->name = $name;
        return $this;
    }

    public function getName(): String {
        return $this->name;
    }

    public function setType(String $type): self {
        $this->type = $type;
        return $this;
    }

    public function getType(): String {
        return $this->type;
    }

    public function setResult(String $result): self {
        $result = str_replace("%", "", $result);
        $result = (int)$result;
        $this->result = $result;
        return $this;
    }

    public function getResult(): int {
        return $this->result;
    }

    public function build(): Subject {
        return (new Subject($this));
    }
}

<?php

namespace App\Services;

use App\Model\Student;

class CalculatePlusScore {
    const ADVANCED_LEVEL = 50;
    const B2 = 28;
    const C2 = 40;


    public function calc(Student $student) {
        $result = $this->calcAdvancedLevel($student->getSubjects());
        if ($student->getLanguageExams()) {
            $result += $this->calcLanguageExam($student->getLanguageExams());
        }
        return $result > 100 ? 100 : $result;
    }

    private function calcAdvancedLevel(array $subjects): int {
        $i = 0;
        foreach ($subjects as $subject) {
            if ($subject->getType() == "emelt") {
                $i += 50;
            }
        }
        return $i;
    }

    private function calcLanguageExam(array $languages): int {
        $arr = [];
        foreach ($languages as $lang) {
            $r = $lang->getType() == "C2" ?  self::C2 : self::B2;
            if (array_key_exists($lang->getName(), $arr))
                $arr[$lang->getName()] = $arr[$lang->getName()] < $r ? $r : $arr[$lang->getName()];
            else
                $arr[$lang->getName()] = $r;
        }
        return array_sum($arr) ?? 0;
    }
}

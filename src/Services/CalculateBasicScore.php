<?php 

namespace App\Services;

use App\Model\Student;

class CalculateBasicScore
{
    private array $requirements;

    public function __construct(array $requirements) {
        $this->requirements = $requirements;
    }

    public function calc(Student $student){
        return ($this->checkBest($student) + $this->checkBest($student, "optional"))*2;
    }

    private function checkBest(Student $s, $type = "required"): int {
        $university = $s->getTargetUniversity();
        $array = $this->requirements[$university->getName()][$university->getFaculty()][$type];
        $result = 0;
    
        foreach ($array as $value) {
            foreach ($s->getSubjects() as $subject) {
                if ($subject->getName() == $value["name"]) {

                    if ($result == null) {
                        $result = $subject->getResult();
                    }
                    elseif ($subject->getResult() > $result) {
                        $result = $subject->getResult();
                    }
                }
            }
        }
        return $result;
    }
}

<?php 

namespace App\Services;

use App\Interfaces\ValidatorInterface;
use App\Model\Student;

class ValidatorService implements ValidatorInterface
{
    /**
     * @var Array<ValidatorInterface>
     */
    private $validators = [];

    public function isValid(Student $student): bool
    {
        foreach ($this->validators as  $validator) {
            if (!$validator->isValid($student)) {
                return false;
            }
        }
        return true;
    }

    public function addValidator(ValidatorInterface $validator) :self
    {
        $this->validators[] = $validator;

        return $this;
    }
}

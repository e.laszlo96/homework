<?php

namespace App;

use App\Model\Result;
use App\Services\CalculateBasicScore;
use App\Services\CalculatePlusScore;
use App\Services\StudentCreator;
use App\Services\ValidatorService;
use App\Validator\BasicRequiredSubjectValidator;
use App\Validator\CheckMinResultValidator;
use App\Validator\UniversityRequirementsValidator;

class Application {
    private array $config;

    public function __construct(array $config) {
        $this->config = $config;
    }

    public function start(array $data) {
        $request = StudentCreator::do($data);
        try {
            (new ValidatorService())
                ->addValidator((new BasicRequiredSubjectValidator()))
                ->addValidator((new CheckMinResultValidator()))
                ->addValidator((new UniversityRequirementsValidator($this->config["university"])))
                ->isValid($request);

            return (new View())->emit(new Result(
                (new CalculateBasicScore($this->config["university"]))->calc($request),
                (new CalculatePlusScore())->calc($request)
            ));
            
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}

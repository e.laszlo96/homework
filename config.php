<?php

$university = [
    "ELTE" => [
        "IK" => [
            "required" => [
                [
                    "name" => "matematika",
                    "level" => "közép"
                ]
            ],
            "optional" => [
                [
                    "name" => "biológia",
                    "level" => "közép"
                ],
                [
                    "name" => "fizika",
                    "level" => "közép"
                ],
                [
                    "name" => "kémia",
                    "level" => "közép"
                ],
                [
                    "name" => "informatika",
                    "level" => "közép"
                ]
            ]
        ]
    ],
    "PPKE" => [
        "BTK" => [
            "required" => [
                [
                    "name" => "angol",
                    "level" => "emelt"
                ]
            ],
            "optional" => [
                [
                    "name" => "francia",
                    "level" => "közép"
                ],
                [
                    "name" => "német",
                    "level" => "közép"
                ],
                [
                    "name" => "olasz",
                    "level" => "közép"
                ],
                [
                    "name" => "orosz",
                    "level" => "közép"
                ],
                [
                    "name" => "spanyol",
                    "level" => "közép"
                ],
                [
                    "name" => "történelem",
                    "level" => "közép"
                ]
            ]
        ]
    ]
];

$config["university"] = $university;